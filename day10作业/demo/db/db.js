// 1. 导入mongodb
var mongodb = require("mongodb");

// 2. 创建MongoClient对象
var MongoClient = mongodb.MongoClient;

// 3.  声明mongodbUrl
var dbaseUrl = "mongodb://127.0.0.1:27017";

// 4. 封装连接mongodb的函数
function connectMongoFn(res, callback) {

    MongoClient.connect(dbaseUrl, function(err, db) {
        if (err) {
            console.log("连接数据库异常:", err);
            res.send({ status: 500, msg: "连接数据库异常, 请查看后端异常信息" })
        } else {
            console.log("连接数据库成功");
            // 创建一个数据库
            var dbase = db.db("P3_2210A");
            callback(dbase, db);
        }
    })
}

module.exports.list = function(jhName, res, obj, callback) {
    // 向数据库中存放数据;
    connectMongoFn(res, function(dbase, db) {

        // 保证传递的参数不是undefined
        obj.find ? "" : obj.find = {};
        obj.sort ? "" : obj.sort = {};
        obj.skip ? obj.skip -= 0 : obj.skip = 0;
        obj.limit ? obj.limit -= 0 : obj.limit = 0;

        dbase.collection(jhName).find(obj.find).sort(obj.sort).skip(obj.skip).limit(obj.limit).toArray(function(err, data) {
            // 查询到数据之后 行 回调函数将 查询到的数据返回;
            callback(err, data);

            db.close();
        })

    });
}


module.exports.count = function(jhName, res, obj, callback) {
    // 向数据库中存放数据;
    connectMongoFn(res, function(dbase, db) {

        // 保证传递的参数不是undefined
        obj.find ? "" : obj.find = {};

        dbase.collection(jhName).count(obj.find).then(function(count) {
            callback(count);

            db.close();
        })

    });
}


module.exports.deleteOne = function(jhName, res, obj, callback) {
    // 向数据库中存放数据;
    connectMongoFn(res, function(dbase, db) {
        dbase.collection(jhName).deleteOne(obj, function(err, data) {
            // 查询到数据之后 行 回调函数将 查询到的数据返回;
            callback(err, data);

            db.close();
        })

    });
}


module.exports.deleteMany = function(jhName, res, obj, callback) {
    // 向数据库中存放数据;
    connectMongoFn(res, function(dbase, db) {

        dbase.collection(jhName).deleteMany(obj, function(err, data) {
            // 查询到数据之后 行 回调函数将 查询到的数据返回;
            callback(err, data);

            db.close();
        })

    });
}

module.exports.delAll = function(jhName, res, callback) {
    connectMongoFn(res, function(dbase, db) {
        dbase.collection(jhName).drop(function(err, data) {
            callback(err, data);
            db.close();
        })
    })
}

module.exports.insertOne = function(jhName, res, obj, callback) {
    // 向数据库中存放数据;
    connectMongoFn(res, function(dbase, db) {

        dbase.collection(jhName).insertOne(obj, function(err, data) {
            // 查询到数据之后 行 回调函数将 查询到的数据返回;
            callback(err, data);

            db.close();
        })

    });
}

module.exports.updateOne = function(jhName, res, qobj, hobj, callback) {
    // 向数据库中存放数据;
    connectMongoFn(res, function(dbase, db) {

        dbase.collection(jhName).updateOne(qobj, hobj, function(err, data) {
            // 查询到数据之后 行 回调函数将 查询到的数据返回;
            callback(err, data);

            db.close();
        })

    });
}