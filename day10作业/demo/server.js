var express = require("express");
var router = require("./router");
var app = express();

app.use(express.static("public"));
app.use(express.static("views"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(router);

app.listen(8044, function() {
    console.log("服务启动了,请求地址是:", "http://127.0.0.1:8044");
})