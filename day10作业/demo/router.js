var express = require("express");
var http = require("./https/http");
var router = express.Router();

router.get("/list", http.list);
router.post("/add", http.add);
router.get("/del", http.del);

module.exports = router;