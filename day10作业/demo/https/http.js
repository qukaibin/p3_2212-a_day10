var db = require("../db/db");
var moment = require("moment");
var mongodb = require("mongodb");
var jhName = "day10";

module.exports.list = function(req, res) {

    db.list(jhName, res, req.query, function(err, data) {
        if (err) {
            res.send({ status: 500, msg: "获取数据失败" })
        } else {
            res.send({ status: 200, msg: "获取数据成功", data: data })
        }
    })
};

module.exports.add = function(req, res) {

    req.body.shijian = moment().format("YYYY-MM-DD HH:mm:ss");

    db.insertOne(jhName, res, req.body, function(err, data) {
        if (err) {
            res.send({ status: 500, msg: "添加数据失败" })
        } else {
            res.send({ status: 200, msg: "添加数据成功", data: data })
        }
    })
};

module.exports.del = function(req, res) {
    var obj = {
        _id: mongodb.ObjectId(req.query.id)
    }

    db.deleteOne(jhName, res, obj, function(err, data) {
        if (err) {
            res.send({ status: 500, msg: "删除数据失败" })
        } else {
            res.send({ status: 200, msg: "删除数据成功", data: data })
        }
    })
};